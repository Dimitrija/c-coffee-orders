﻿using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using coffee_orders.Models;
using coffee_orders.Models.Context;

namespace coffee_orders.Controllers
{
    public class DrinkTypesController : ApiController
    {
        private readonly CoffeeOrdersContext db = new CoffeeOrdersContext();

        // GET: api/DrinkTypes
        public IQueryable GetAll()
        {
            return db.DrinkTypes.Select(dt => new
            {
                dt.Id,
                dt.Name,
                Drinks = dt.Drinks.Select(d => new {d.Id, d.Name, d.Active})
            });
        }


        // GET: api/DrinkTypes/5
        public IQueryable Get(int id)
        {
            var response = db.DrinkTypes.Select(dt => new
                {
                    dt.Id,
                    dt.Name,
                    Drinks = dt.Drinks.Select(d => new {d.Id, d.Name, d.Active})
                })
                .Where(s => s.Id == id);

            return response;
        }


        // POST: api/DrinkTypes
        public IHttpActionResult Save(DrinkType drinkType)
        {
            db.DrinkTypes.AddOrUpdate(drinkType);
            db.SaveChanges();
            return Ok(drinkType);
        }

        // DELETE: api/DrinkTypes/5
        [ResponseType(typeof(DrinkType))]
        public IHttpActionResult Delete(int id)
        {
            var drinkType = db.DrinkTypes.Find(id);
            if (drinkType == null)
                return NotFound();

            db.DrinkTypes.Remove(drinkType);
            db.SaveChanges();

            return Ok(drinkType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
    }
}