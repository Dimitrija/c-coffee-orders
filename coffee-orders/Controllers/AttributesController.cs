﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Http;
using coffee_orders.Models.Context;
using Attribute = coffee_orders.Models.Attribute;

namespace coffee_orders.Controllers
{
    public class AttributesController : ApiController
    {
        private readonly CoffeeOrdersContext db = new CoffeeOrdersContext();

        public List<Attribute> GetAll()
        {
            var query = (from a in db.Attributes select a).ToList();

            return query;
        }

        public Attribute Get(int id)
        {
            var query = (from a in db.Attributes where a.Id == id select a).SingleOrDefault();

            return query;
        }

        public Attribute Save(Attribute attribute)
        {
            if (attribute.Id == 0)
            {
                db.Attributes.Add(attribute);
                db.SaveChanges();
            }
            else
            {
                var query = (from a in db.Attributes where a.Id == attribute.Id select a).SingleOrDefault();
                query.Name = attribute.Name;
                query.Active = attribute.Active;
                db.SaveChanges();
            }
            

            return attribute;
        }

        public bool Delete(int id)
        {
            var attribute = db.Attributes.Find(id);
            db.Attributes.Remove(attribute);
            db.SaveChanges();

            return true;
        }
    }
}