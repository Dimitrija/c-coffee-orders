﻿using System;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using coffee_orders.Models;
using coffee_orders.Models.Context;

namespace coffee_orders.Controllers
{
    public class DrinksController : ApiController
    {
        private readonly CoffeeOrdersContext db = new CoffeeOrdersContext();

        // GET: api/Drinks
        public IQueryable GetAll()
        {
            return db.Drinks.Select(x => new {x.Name, x.Id, DrinkName = x.DrinkType.Name});
        }

        // GET: api/DrinkTypes/5
        public IQueryable Get(int id)
        {
            return db.Drinks.Select(x => new {x.Name, x.Id, DrinkName = x.DrinkType.Name}).Where(s => s.Id == id);
        }

        // POST: api/Drink
        [ResponseType(typeof(Drink))]
        public IHttpActionResult Save(Drink drink)
        {
            db.Drinks.AddOrUpdate(drink);
            db.SaveChanges();
            return Ok(drink);
        }

        // DELETE: api/Drink/5
        [ResponseType(typeof(Drink))]
        public IHttpActionResult Delete(int id)
        {
          
            var drink = db.Drinks.Find(id);
            if (drink == null)
                return NotFound();

            db.Drinks.Remove(drink);
            db.SaveChanges();
            return Ok(drink);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
    }
}