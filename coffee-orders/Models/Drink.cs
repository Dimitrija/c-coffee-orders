﻿using System.ComponentModel.DataAnnotations.Schema;

namespace coffee_orders.Models
{
    public class Drink
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public int DrinkTypeId { get; set; }

        [ForeignKey("DrinkTypeId")]
        public virtual DrinkType DrinkType { get; set; }
    }
}