﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace coffee_orders.Models
{
    public class Attribute
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
    }
}