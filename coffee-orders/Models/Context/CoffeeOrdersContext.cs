﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace coffee_orders.Models.Context
{
    public class CoffeeOrdersContext : DbContext
    {
        public CoffeeOrdersContext() : base("name = CoffeeOrdersConnection")
        {
            
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Drink> Drinks { get; set; }
        public DbSet<DrinkType> DrinkTypes { get; set; }
        public DbSet<Attribute> Attributes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DrinkType>()
                .HasMany(d => d.Drinks);

        }
    }
}