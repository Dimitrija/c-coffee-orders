﻿using System.Collections.Generic;

namespace coffee_orders.Models
{

    public class DrinkType
    {
        public DrinkType()
        {
           
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Drink> Drinks { get; set; }
    }
}